<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            "ulid" => [
                "type" => "CHAR",
                "constraint" => 26
            ],
            "user_id" => [
                "type" => "VARCHAR",
                "constraint" => 15,
            ],
            "name" => [
                "type" => "VARCHAR",
                "constraint" => 20,
            ],
            "password" => [
                "type" => "CHAR",
                "constraint" => 97,
            ],
            "is_admin" => [
                "type" => "BOOLEAN",
                "default" => false,
            ],
            "created_at" => [
                "type" => "TIMESTAMP",
            ],
            "updated_at" => [
                "type" => "TIMESTAMP",
            ],
            "deleted_at" => [
                "type" => "TIMESTAMP",
                "null" => true,
            ]
        ]);
        $this->forge->addKey(["ulid", "user_id"]);
        $this->forge->addPrimaryKey("ulid");
        $this->forge->addUniqueKey("user_id");
        $this->forge->createTable("users");
    }

    public function down()
    {
        $this->forge->dropTable("users");
    }
}
