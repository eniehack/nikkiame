<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateInvitationsTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            "id" => [
                "type" => "CHAR",
                "constraint" => 26,
                "unique" => true,
            ],
            "available_times" => [
                "type" => "INT",
                "default" => 1,
            ],
            "is_expired" => [
                "type" => "BOOLEAN",
                "default" => false,
            ],
            "expired_at" => [
                "type" => "TIMESTAMP",
            ],
            "created_at" => [
                "type" => "TIMESTAMP",
            ],
            "updated_at" => [
                "type" => "TIMESTAMP",
            ]
        ]);
        $this->forge->addPrimaryKey("id");
        $this->forge->addKey("id");
        $this->forge->createTable("invitations");
    }

    public function down()
    {
        $this->forge->dropTable("invitations");
    }
}
