<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreatePostsTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            "id" => [
                "type" => "CHAR",
                "constraint" => 26,
            ],
            "author" => [
                "type" => "CHAR",
                "constraint" => 26,
            ],
            "title" => [
                "type" => "VARCHAR",
                "constraint" => 20,
            ],
            "content" => [
                "type" => "TEXT",
            ],
            "scope" => [
                "type" => "INT",
                "unsigned" => true,
            ],
            "is_draft" => [
                "type" => "BOOLEAN",
                "default" => false,
            ],
            "created_at" => [
                "type" => "TIMESTAMP",
            ],
            "updated_at" => [
                "type" => "TIMESTAMP",
            ],
            "deleted_at" => [
                "type" => "TIMESTAMP",
                "null" => true,
            ]
        ]);
        $this->forge->addKey(["id", "author"]);
        $this->forge->addPrimaryKey("id");
        $this->forge->addForeignKey("author", "users", "ulid", "CASCADE", "CASCADE");

        $this->forge->createTable("posts");
    }

    public function down()
    {
        $this->forge->dropTable("posts");
    }
}
