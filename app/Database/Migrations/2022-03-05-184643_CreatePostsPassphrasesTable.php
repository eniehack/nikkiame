<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreatePostsPassphrasesTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            "post_id" => [
                "type" => "CHAR",
                "constraint" => 26,
            ],
            "passphrase" => [
                "type" => "CHAR",
                "constraint" => 97,
            ],
            "updated_at" => [
                "type" => "TIMESTAMP",
            ],
        ]);
        $this->forge->addKey(["post_id"]);
        $this->forge->addPrimaryKey("post_id");
        $this->forge->addForeignKey("post_id", "posts", "id", "CASCADE", "CASCADE");

        $this->forge->createTable("posts_passphrases");
    }

    public function down()
    {
        $this->forge->dropTable("posts_passphrases");
    }
}
