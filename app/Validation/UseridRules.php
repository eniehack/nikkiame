<?php

namespace App\Validation;

class UseridRules
{
    public function user_id(string $user_id): bool
    {
        $status = preg_match("/[^0-9a-z._]+/", $user_id);
        if ( $status === 0 ) {
            return true;
        } else {
            return false;
        }
    }
}
