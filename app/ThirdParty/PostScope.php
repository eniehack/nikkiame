<?php

namespace App\ThirdParty;

use MyCLabs\Enum\Enum;

final class PostScope extends Enum {
    private const Public = 0;
    private const Passworded = 1;
}
