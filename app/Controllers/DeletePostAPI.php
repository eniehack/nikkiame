<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Posts;
use App\Models\Users;
use CodeIgniter\API\ResponseTrait;

class DeletePostAPI extends BaseController
{
    use ResponseTrait;
    public function index(string $post_id)
    {
        $session = session();

        if (!$session->has("user_id")) {
            return $this->failForbidden("you need to sign in");
        }

        $user_id = $session->user_id;

        $userModel = new Users();
        $user = $userModel->find($user_id);

        if ( ! isset($user) ) {
            return $this->failForbidden("you need to sign in");
        }

        $postModel = new Posts();
        $status = $postModel->delete($post_id);
        if ($status === false) {
            return $this->fail("failed to delete");
        }

        return $this->RespondDeleted();
    }
}
