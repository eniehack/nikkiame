<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Posts;
use App\Models\PostsPassphrases;
use App\Models\Users;
use App\ThirdParty\PostScope;
use BadMethodCallException;
use Ulid\Ulid;

class CreatePosts extends BaseController
{
    public function index()
    {
        $session = session();

        if (!$session->has("user_id")) {
            return $this->response
                ->setStatusCode(403);
        }

        $user_id = $session->user_id;


        $userModel = new Users();
        $user = $userModel->find($user_id);

        if ( ! isset($user) ) {
            return $this->response
                ->setStatusCode(403);
        }

        $this->response->CSP->upgradeInsecureRequests(true);
        $this->response->CSP->addScriptSrc("unpkg.com", false);
        $this->response->CSP->addScriptSrc("'unsafe-eval'", false);
        $this->response->CSP->addScriptSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addStyleSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addStyleSrc("fonts.googleapis.com", false);
        $this->response->CSP->addFontSrc("cdn.jsdelivr.net", false);

        helper("form");
        return $this->response
            ->setBody(
                view('createpost', [
                    "session_user" => $user_id,
                ])
            );
    }

    public function save() {
        $session = session();
        $this->response->CSP->addScriptSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addStyleSrc("cdn.jsdelivr.net", false);

        if (!$session->has("user_id")) {
            return $this->response
                ->setStatusCode(403);
        }

        $user_id = $session->user_id;

        $userModel = new Users();
        $user = $userModel->find($user_id);

        if ( ! isset($user) ) {
            return $this->response
                ->setStatusCode(403);
        }

        if ($this->validate([
                "title" => "required|max_length[20]",
                "content" => "required",
                "scope" => "required"
            ])) {

            $requested_scope = (int)$this->request->getPost("scope");

            try {
                $scope = PostScope::from($requested_scope);
            } catch (BadMethodCallException $e) {
                return $this->response
                    ->setStatusCode(400);
            }

            $ulid = (string) new Ulid();
            $postsModel = new Posts();
            $postsModel->protect(false);
            $postsModel->insert([
                "id" => $ulid,
                "author" => $session->user_id,
                "content" => $this->request->getPost("content"),
                "title" => $this->request->getPost("title"),
                "scope" => $scope,
            ]);
            $postsModel->protect(true);

            if ( $scope->equals( PostScope::Passworded() ) ) {
                $postsPassphraseModel = new PostsPassphrases();
                $postsPassphraseModel->protect(false);
                $postsPassphraseModel->insert([
                     "post_id" => $ulid,
                     "passphrase" => $this->request->getPost("passphrase"),
                ]);
                $postsPassphraseModel->protect(true);
            }

            return $this->response
                ->redirect("/@{$user['user_id']}/p/{$ulid}");
        } else {
            return $this->response
                ->setStatusCode(400);

        }

    }
}
