<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Posts;
use App\Models\PostsPassphrases;
use App\Models\Users;
use App\ThirdParty\PostScope;
use BadMethodCallException;
use Ulid\Ulid;

class EditPosts extends BaseController
{
    public function index(string $user_id, string $post_id)
    {
        $session = session();

        if (!$session->has("user_id")) {
            return $this->response
                ->setStatusCode(403);
        }

        $user_id = $session->user_id;

        $userModel = new Users();
        $user = $userModel->find($user_id);

        if ( ! isset($user) ) {
            return $this->response
                ->setStatusCode(404)
                ->setBody("not found");
        }

        $postModel = new Posts();
        $query = $postModel->builder()
                   ->join("posts_passphrases", "posts.id = posts_passphrases.post_id", "left")
                   ->where("id", $post_id)
                   ->get();
        $post = $query->getFirstRow("array");

        if ( $user["ulid"] !== $post["author"] ) {
            return $this->response
                ->setStatusCode(404)
                ->setBody("not found");
        }

        $this->response->CSP->upgradeInsecureRequests(true);
        $this->response->CSP->addScriptSrc("unpkg.com", false);
        $this->response->CSP->addScriptSrc("'unsafe-eval'", false);
        $this->response->CSP->addScriptSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addStyleSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addFontSrc("cdn.jsdelivr.net", false);

        helper("form");
        return $this->response
            ->setBody(
                view('editpost', [
                         "form" => [
                             "title" => $post["title"],
                             "content" => $post["content"],
                             "scope" => PostScope::from( intval($post["scope"]) ),
                             "passphase" => $post["passphrase"],
                         ],
                         "url" => "/@{$user['user_id']}/p/{$post_id}/edit"
                     ],
                )
            );
    }

    public function save(string $user_id, string $post_id) {
        $session = session();
        $this->response->CSP->addScriptSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addStyleSrc("cdn.jsdelivr.net", false);

        if (!$session->has("user_id")) {
            return $this->response
                ->setStatusCode(403);
        }

        $user_id = $session->user_id;

        $userModel = new Users();
        $user = $userModel->find($user_id);

        if ( ! isset($user) ) {
            return $this->response
                ->setStatusCode(403);
        }

        if ($this->validate([
                "title" => "required|max_length[20]",
                "content" => "required",
                "scope" => "required"
            ])) {

            $requested_scope = (int)$this->request->getPost("scope");

            try {
                $scope = PostScope::from($requested_scope);
            } catch (BadMethodCallException $e) {
                return $this->response
                    ->setStatusCode(400);
            }

            $postsModel = new Posts();
            $status = $postsModel->save([
                        "id" => $post_id,
                        "content" => $this->request->getPost("content"),
                        "title" => $this->request->getPost("title"),
                        "scope" => $scope,
                    ]);
            if ($status === false) {
                return $this->response
                    ->setStatusCode(500)
                    ->setBody("internal server error");
            }
            unset($status);

            if ( $scope->equals( PostScope::Passworded() ) ) {
                $postsPassphraseModel = new PostsPassphrases();
                $status = $postsPassphraseModel->save([
                            "post_id" => $post_id,
                            "passphrase" => $this->request->getPost("passphrase"),
                        ])
                        ->update();
                if ($status === false) {
                    return $this->response
                        ->setStatusCode(500)
                        ->setBody("internal server error");
                }
            }

            return $this->response
                ->redirect("/@{$user['user_id']}/p/{$post_id}");
        } else {
            return $this->response
                ->setStatusCode(400);

        }

    }
}
