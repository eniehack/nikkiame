<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Posts;
use App\Models\PostsPassphrases;
use App\Models\Users;
use App\ThirdParty\PostScope;
use League\CommonMark\CommonMarkConverter;

class ReadPosts extends BaseController
{
    public function index(string $user_id, string $post_id)
    {
        $this->response->CSP->addScriptSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addScriptSrc("'inline'", false);
        $this->response->CSP->addStyleSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addStyleSrc("fonts.googleapis.com", false);
        $this->response->CSP->addFontSrc("cdn.jsdelivr.net", false);

        $userModel = new Users();
        $user = $userModel->where("user_id", $user_id)->first();
        if (! isset($user)) {
           return $this->response
               ->setStatusCode(404)
               ->setBody(
                   "not found"
               );
        }

        $postModel = new Posts();
        $post = $postModel->find($post_id);
        if (! isset($post) ) {
           return $this->response
               ->setStatusCode(404)
               ->setBody(
                   "not found"
               );
        }
        $scope = PostScope::from((int)$post["scope"]);

        if ( $scope->equals( PostScope::Passworded() ) ) {
            $postPassphraseModel = new PostsPassphrases();
            $passphrase = $postPassphraseModel->find($post_id);
            if ( ! password_verify($this->request->getGet("pass"), $passphrase["passphrase"]) ) {
                return $this->response
                    ->setStatusCode(400)
                    ->setBody("incorrect password");
            }
        }

        $converter = new CommonMarkConverter();
        $session = session();

        helper("url");
        return $this->response
            ->setBody(
                view("posts_detail", [
                    "converter" => $converter,
                    "post" => $post,
                    "user" => $user,
                    "session_user" => $session->get("user_id"),
                ])
            );
    }
}
