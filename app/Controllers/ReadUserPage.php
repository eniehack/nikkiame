<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Posts;
use App\Models\PostsPassphrases;
use App\Models\Users;
use App\ThirdParty\PostScope;
use League\CommonMark\CommonMarkConverter;

class ReadUserPage extends BaseController
{
    public function index(string $user_id)
    {
        $this->response->CSP->addScriptSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addStyleSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addStyleSrc("fonts.googleapis.com", false);
        $this->response->CSP->addFontSrc("cdn.jsdelivr.net", false);

        $userModel = new Users();
        $user = $userModel->where("user_id", $user_id)->first();
        if (! isset($user)) {
        return $this->response
            ->setStatusCode(404)
            ->setBody(
                "not found"
            );
        }

        $session = session();
        $postModel = new Posts();
        if ($session->has("user_id") && ($session->get("user_id") === $user["ulid"])) {
            $posts = $postModel->where(["author" => $user["ulid"]])->findAll();
        } else {
            $posts = $postModel->where(["author" => $user["ulid"], "scope" => PostScope::Public()->getValue()])->findAll();
        }

        helper("url");
        return $this->response
            ->setBody(
                view("userpage", [
                    "posts" => $posts,
                    "user" => $user,
                ])
            );
    }

    public function feed(string $user_id) {
        $userModel = new Users();
        $user = $userModel->where("user_id", $user_id)->first();
        if (! isset($user)) {
        return $this->response
            ->setStatusCode(404)
            ->setBody(
                "not found"
            );
        }

        $postModel = new Posts();
        $posts = $postModel->where(["author" => $user["ulid"], "scope" => PostScope::Public()->getValue()])
                           ->orderBy("created_at","DESC")
                           ->limit(10)
                           ->find();

        helper("url");
        return $this->response
            ->setContentType("application/xml+atom")
            ->setBody(
                view("post.atom.xml.php", [
                    "posts" => $posts,
                    "user" => $user,
                    "converter" => new CommonMarkConverter(),
                ])
            );
    }
}
