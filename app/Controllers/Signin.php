<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Users;

class Signin extends BaseController
{
    public function index()
    {
        $this->response->CSP->addScriptSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addStyleSrc("cdn.jsdelivr.net", false);
        helper("form");
        return view("signin");
    }

    public function check() {
        $session = session();
        $this->response->CSP->addScriptSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addStyleSrc("cdn.jsdelivr.net", false);

        $usersModel = new Users();
        $user = $usersModel->select('ulid, password')
            ->where("user_id", $this->request->getPost("user_id"))
            ->first();
        if ( !isset($user) ) {
            return $this->response
                ->setStatusCode(400)
                ->sendBody("not found");
        }

        if ( !password_verify($this->request->getPost("password"), $user["password"]) ) {
            return $this->response
                ->setStatusCode(400)
                ->sendBody("incorrect password");
        }

        $session->set(["user_id" => $user["ulid"]]);

        return $this->response
            ->setStatusCode(200)
            ->redirect("/");
    }
}
