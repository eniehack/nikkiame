<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Posts;
use App\Models\FollowingStates;
use App\Models\Users;
use App\ThirdParty\PostScope;
use DateTime;
use function var_dump;

class Timeline extends BaseController
{
    public function local()
    {
        $cache = cache();
        $session = session();
        $this->response->CSP->setDefaultSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addScriptSrc("'inline'", false);
        $this->response->CSP->addStyleSrc("fonts.googleapis.com", false);

        if ($session->has("user_id")) {
            $session_user = $session->user_id;
        }

        $userModel = new Users();
        $users = $userModel->select("user_id, name, ulid")
                    ->findAll();
        $postModel = new Posts();
        $posts = [];
        $user_dict = [];
        foreach ($users as $user) {
            /*
            $user_posts = $cache->get("posts_{$user["user_id"]}");
            if ($user_posts === null) {
            */
                $user_posts = $postModel->where("author", $user["ulid"])
                                        ->where("scope", PostScope::Public()->getValue())
                                        ->orderBy("updated_at", "DESC")
                                        ->limit(3)
                                        ->find();
                $user_dict[$user["user_id"]] = $user;
            /*
                $cache->save("posts_{$user["user_id"]}", $user_posts);
            }
            */

            if ( isset($user_posts[0]) ) {
                $posts[$user["user_id"]] = $user_posts;
            }
            unset($user_posts);
        }

        uasort($posts, function ($val1, $val2) {
            $time1 = new DateTime($val1[0]["updated_at"]);
            $time2 = new DateTime($val2[0]["updated_at"]);
            $interval = $time1->diff($time2);
            // NOTE: 新しいものが上に来る = $time1が古ければ後にする必要がある
            if ($interval->invert === 0) {
                return 1;
            } else {
                return -1;
            }
        });

        helper("url");
        return $this->response
            ->setBody(
                view("timeline", [
                    "posts" => $posts,
                    "users" => $user_dict,
                    "session_user" => $session_user,
                ])
            );
    }
}
