<?php

namespace App\Controllers;

use App\Models\Invitations;
use App\Models\Users;
use Config\App;
use DateTime;

class Signup extends BaseController
{
    public function index()
    {
        $config = new App();
        $this->response->CSP->addScriptSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addStyleSrc("cdn.jsdelivr.net", false);

        if ($config->openRegister) {
            helper("form");
            return view('signup', [
                        "invitation_id" => null,
                ]);

        } elseif ($config->openInvitationRegister) {
            $invitation_id = $this->request->getGet("invitation_id");

            if (! isset($invitation_id) ) {
                return $this->response
                    ->setStatusCode(403);
            }

            $model = model(Invitations::class);
            $invitation = $model->find($invitation_id);
            if (!isset($invitation)) {
                return $this->response
                    ->setStatusCode(403);
            }

            $expired_at = new DateTime($invitation["expired_at"]);
            $now = new DateTime();

            if ($expired_at < $now || $invitation["is_expired"] === true ) {
                return $this->response
                    ->setStatusCode(403);
            }

            helper("form");
            return $this->response
                ->setStatusCode(200)
                ->appendBody(
                    view('signup', [
                        "invitation_id" => $invitation["id"],
                    ])
                );
        } else {
           return $this->response
               ->setStatusCode(404);
        }
    }

    public function create() {
        $user = model(Users::class);
        $invitationModel = new Invitations();
        $config = new App();
        $this->response->CSP->addScriptSrc("cdn.jsdelivr.net", false);
        $this->response->CSP->addStyleSrc("cdn.jsdelivr.net", false);

        if ($config->openRegister) {
            if ($this->request->getMethod() === "post" &&
                $this->validate([
                    "user_id" => "required|min_length[1]|max_length[15]|user_id",
                    "name" => "required|max_length[20]",
                    "password" => "required",
                ])) {
                $user->save([
                    "user_id" => $this->request->getPost("user_id"),
                    "name" => esc($this->request->getPost("name")),
                    "password" => $this->request->getPost("password"),
                ]);
                return $this->response
                    ->setStatusCode(201)
                    ->redirect("/signin");
                // TODO: loginに誘導して「loginしてください」みたいなメッセージが出せるといいかも
            } else {
                return $this->response
                    ->setStatusCode(400)
                    ->sendBody(
                        view('signup')
                    );
            }
        } elseif ($config->openInvitationRegister) {
            if ($this->request->getMethod() === "post" &&
                $this->validate([
                    "user_id" => "required|min_length[1]|max_length[15]|user_id",
                    "name" => "required|max_length[20]",
                    "password" => "required",
                    "invitation_id" => "required|exact_length[26]"
                ])) {

                $invitation = $invitationModel->asObject()->find($this->request->getPost("invitation_id"));
                $invitation->is_expired = true;
                $invitation->available_times = $invitation->available_times - 1;
                $invitationModel->update($invitation->id, $invitation);

                $user->save([
                    "user_id" => strtolower($this->request->getPost("user_id")),
                    "name" => esc($this->request->getPost("name")),
                    "password" => $this->request->getPost("password"),
                ]);

                return $this->response
                    ->setStatusCode(201)
                    ->redirect("/signin");
                // TODO: loginに誘導して「loginしてください」みたいなメッセージが出せるといいかも
            }
        } else {
           return $this->response
               ->setStatusCode(404);
        }

    }
}
