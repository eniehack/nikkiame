<?php

namespace App\Commands;

use App\Models\Users;
use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\CLI;
use CodeIgniter\HTTP\CLIRequest;
use Config\App;
use DateTime;
use Ulid\Ulid;

class Useradd extends BaseCommand
{
    /**
     * The Command's Group
     *
     * @var string
     */
    protected $group = 'Nikkiame User Management';

    /**
     * The Command's Name
     *
     * @var string
     */
    protected $name = 'user:add';

    /**
     * The Command's Description
     *
     * @var string
     */
    protected $description = 'add a user';

    /**
     * The Command's Usage
     *
     * @var string
     */
    protected $usage = 'command:name [arguments] [options]';

    /**
     * The Command's Arguments
     *
     * @var array
     */
    protected $arguments = [
        "username" => "",
    ];

    /**
     * The Command's Options
     *
     * @var array
     */
    protected $options = [
        "--admin" => "add a user as a administrator.",
        "-p" => "password [REQUIRED]",
        "-i" => "user_id [REQUIRED]",
    ];

    /**
     * Actually execute a command.
     *
     * @param array $params
     */
    public function run(array $params)
    {
        $config = new App();
        $password = $params["p"] ?? CLI::getOption("p");
        $alias_id = $params["i"] ?? CLI::getOption("i");
        $name = $params[0];

        if (! isset($password) ) {
            CLI::write("password(-p) is required");
            exit(1);
        }

        if (! isset($alias_id) ) {
            CLI::write("user_id(-i) is required");
            exit(1);
        }

        if (! isset($name) ) {
            CLI::write("username is required");
            exit(1);
        }

        if (array_key_exists("admin", $params) || CLI::getOption("admin")) {
            $is_admin = true;
        } else {
            $is_admin = false;
        }
        $now = new DateTime();

        try {
            $model = model(Users::class);
            $builder = $model->builder();
            $status = $builder->set("user_id", $alias_id)
                              ->set("ulid", (string) new Ulid())
                              ->set("created_at", $now->format(DateTime::ATOM))
                              ->set("updated_at", $now->format(DateTime::ATOM))
                              ->set("name", $name)
                              ->set("is_admin", $is_admin)
                              ->set("password", password_hash($password, PASSWORD_ARGON2ID))
                              ->insert();
        } catch (\Exception $e) {
            $this->showError($e);
            exit(1);
        }

        if ($status) {
            exit(0);
        } else {
            exit(1);
        }
    }
}
