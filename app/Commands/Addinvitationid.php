<?php

namespace App\Commands;

use App\Models\Invitations;
use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\CLI;
use Config\App;
use DateTime;
use Ulid\Ulid;

class Addinvitationid extends BaseCommand
{
    /**
     * The Command's Group
     *
     * @var string
     */
    protected $group = 'Invitation';

    /**
     * The Command's Name
     *
     * @var string
     */
    protected $name = 'invitation:create';

    /**
     * The Command's Description
     *
     * @var string
     */
    protected $description = 'create invitation id.';

    /**
     * The Command's Usage
     *
     * @var string
     */
    protected $usage = 'invitation:create [options]';

    /**
     * The Command's Arguments
     *
     * @var array
     */
    protected $arguments = [];

    /**
     * The Command's Options
     *
     * @var array
     */
    protected $options = [
        "--limit" => "available times ( 0)",
        "--expired-at" => "when invitation token expired ( +1day)"
    ];

    /**
     * Actually execute a command.
     *
     * @param array $params
     */
    public function run(array $params)
    {
        $limit = $params["limit"] ?? CLI::getOption("limit");
        if (! isset($limit) ) {
            $limit = 1;
        }


        $expired_at = $params["expired-at"] ?? CLI::getOption("expired-at");
        echo var_dump($expired_at);
        if ( !isset($expired_at) ) {
            $expired_at = new DateTime();
            $expired_at->modify("+1 day");
        }

        $invitation_id = (string) new Ulid();

        try {
            $now = new DateTime();
            $model = model(Invitations::class);
            $builder = $model->builder();
            $status = $builder->set("available_times", $limit)
                              ->set("id", $invitation_id)
                              ->set("created_at", $now->format(DateTime::ATOM))
                              ->set("updated_at", $now->format(DateTime::ATOM))
                              ->set("expired_at", $expired_at->format(DateTime::ATOM))
                              ->insert();
        } catch (\Exception $e) {
            $this->showError($e);
            exit(1);
        }

        $config = new App();
        $baseurl = $config->baseURL;
        if ($status) {
            CLI::write($baseurl ."/signup?invitation_id=" . $invitation_id);
            CLI::newLine();
            exit(0);
        } else {
            exit(1);
        }
    }
}
