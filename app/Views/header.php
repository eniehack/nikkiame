<header>
    <nav class="ui secondary menu">
        <?= anchor("/", "nikkiame (alpha)", ["class" => "item"]) ?>
        <?= anchor("/posts/new", "new a post", ["class" => "item"]) ?>
        <div class="right menu">
            <?php if ( isset($session_user) === false ): ?>
                <div class="item">
                    <?= anchor("/signin", "sign in", ["class" => "ui primary button"]) ?>
                </div>
            <?php else: ?>
                <div class="item">
                    <?= anchor("/signout", "sign out", ["class" => "ui button"]) ?>
                </div>
            <?php endif; ?>
        </div>
    </nav>
</header>
