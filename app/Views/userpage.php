<?php use App\ThirdParty\PostScope; ?>
<html>
    <head>
     <meta charset="UTF-8"/>
     <title><?= $user["name"] ?></title>
     <link href="<?= site_url("/@${user['user_id']}/feed.xml") ?>" rel="alternate" type="application/atom+xml"/>
	 <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
	 <link href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.css" rel="stylesheet"/>
	 <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.js"></script>
    </head>
    <body>
        <?= $this->include('header') ?>
        <main class="ui container">
            <div class="ui header">
                <h1><?= $user["name"] ?></h1>
                <p class="sub header">@<?= $user["user_id"] ?></p>
            </div>
            <div>
                <div class="ui relaxed devided list">
                    <?php foreach ($posts as $post): ?>
                    <div class="item">
                        <div class="content">
                            <div class="header">
                                <?= anchor("/@{$user['user_id']}/p/{$post['id']}", $post["title"], ["class" => "ui big text"]) ?>
                                <?php if ($post["scope"] === PostScope::Passworded()->getValue() ): ?>
                                    <i class="lock icon"></i>
                                <?php endif; ?>
                            </div>
                            <time class="description"><?= $post["created_at"] ?></time>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </main>
    </body>
</html>
