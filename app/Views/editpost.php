<!doctype html>
<html>

<head>
	<meta charset="UTF-8" />
	<title>new post</title>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.css" rel="stylesheet"/>
	<script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.js"></script>
	<script src="https://unpkg.com/alpinejs@3.9.1/dist/cdn.min.js" defer></script>
</head>

<body>
	<?= session()->getFlashdata("error") ?>
	<?= service("validation")->listErrors() ?>
    <?= $this->include('header') ?>
	<h2>edit post</h2>
	<?= form_open($url, ["class" => "ui form"]) ?>
	<div class="field">
		<?= form_label("title") ?>
		<?= form_input("title", $form["title"]) ?>
	</div>
	<div class="field">
		<?= form_label("content", "content") ?>
		<?= form_textarea("content", $form["content"], ["id" => "content"]) ?>
		<p class="ui small text">
			<a href="">Markdown</a>記法が使えます
		</p>
	</div>
	<div x-data="{scope: '<?= $form["scope"]->getValue() ?>'}" class="grouped fields">
		<label for="scope">select this post's scope</label>
		<div class="field">
			<div class="ui radio checkbox">
				<?= form_radio("scope", \App\ThirdParty\PostScope::Public(), false, ["id" => "public", "x-model" => "scope"]) ?>
				<?= form_label("public") ?>
			</div>
		</div>
		<div class="field">
			<div class="ui radio checkbox">
				<?= form_radio("scope", \App\ThirdParty\PostScope::Passworded(), false, ["id" => "passworded", "x-model" => "scope"]) ?>
				<?= form_label("passworded") ?>
			</div>
		</div>
		<div x-show="scope === '<?= \App\ThirdParty\PostScope::Passworded() ?>'" class="field">
			<label for="passphrase">passphrase</label>
			<?php if ( isset($form["passphrase"]) ) : ?>
				<?= form_input("passphrase", $form["passphrase"]) ?>
			<?php else: ?>
				<?= form_input("passphrase", "") ?>
			<?php endif; ?>
		</div>
	</div>
	<?= form_submit("submit", "submit", ["class" => "ui primary button"]) ?>
	<?= form_close() ?>
</body>

</html>
