<?xml version="1.0" encoding="utf-8"?>
<?php
use Config\App;
$config = new App();
?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title><?= $user["name"] ?> - nikkiame</title>
    <id><?= site_url("/@${user['user_id']}") ?></id>
    <author>
        <name><?= $user["name"] ?></name>
    </author>
    <updated><?php
        $updated = new DateTime($posts[0]["updated_at"]);
        $updated->setTimezone(timezone_open("UTC"));
        echo $updated->format(DateTime::ATOM);
        unset($updated);
    ?></updated>
    <generator uri="<?= $config->repoURL ?>">nikkiame</generator>
    <link rel="self" type="application/atom+xml" href="<?= site_url("/@${user["user_id"]}/feed.xml") ?>"/>
    <link rel="alternate" type="text/html" href="<?= site_url("/@${user["user_id"]}") ?>"/>
    <?php foreach($posts as $post): ?>
    <?php
    $created = new DateTime($post["created_at"]);
    $created->setTimezone(timezone_open("UTC"));
    $updated = new DateTime($post["updated_at"]);
    $updated->setTimezone(timezone_open("UTC"));
    ?>
    <entry>
        <title><?= $post["title"] ?></title>
        <id>tag:<?= parse_url($config->baseURL, PHP_URL_HOST) ?>,<?= $created->format("Y-m-d") ?>:<?= "/@${user["user_id"]}/p/${post["id"]}" ?></id>
        <author>
            <name><?= $user["name"] ?></name>
        </author>
        <link rel="alternate" type="text/html" href="<?= site_url("/@${user["user_id"]}/p/${post["id"]}") ?>" />
        <updated><?= $updated->format(DateTime::ATOM) ?></updated>
        <published><?= $created->format(DateTime::ATOM) ?></published>
        <content type="html">
           <?= esc( $converter->convertToHtml($post["content"]) ) ?>
        </content>
    </entry>
    <?php
        unset($created);
        unset($updated);
    ?>
    <?php endforeach; ?>
</feed>
