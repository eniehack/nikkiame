<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>local timeline</title>
	    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
	    <link href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.css" rel="stylesheet"/>
	    <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.js"></script>
    </head>
    <body>
        <?= $this->include("header") ?>
        <main class="ui container">
            <h1 class="header">latest posts in this server</h1>
            <div class="ui list">
            <?php foreach ( $posts as $user => $value ): ?>
                <div class="item">
                    <h2 class="header">
                        <?= anchor("/@${user}", $users[$user]["name"]) ?>
                    </h2>
                    <span>
                        @<?= $user ?>
                    </span>
                    <div class="ui list">
                    <?php foreach ( $value as $post ): ?>
                        <div class="item">
                            <?= anchor("/@${user}/p/${post['id']}", $post["title"], ["class" => "header"]) ?>
                            <?= $post["created_at"] ?>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </main>
    </body>
</html>
