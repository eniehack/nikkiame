<!doctype html>
<html>

<head>
	<meta charset="UTF-8" />
	<title>sign in</title>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.css" rel="stylesheet"/>
	<script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.js"></script>
</head>

<body>
	<?= session()->getFlashdata("error") ?>
	<?= service("validation")->listErrors() ?>
    <?= $this->include('header') ?>
	<h2>sign in</h2>
	<?= form_open("/signin", ["class" => "ui form"]) ?>
	<div class="field">
		<?= form_label("user_id") ?>
		<?= form_input("user_id", "", ["autocomplete" => "username"]) ?>
	</div>
	<div class="field">
		<?= form_label("password") ?>
		<?= form_password("password", "", ["autocomplete" => "current-password"]) ?>
	</div>
	<?= form_submit("submit", "sign in", ["class" => "ui primary button"]) ?>
	<?= form_close() ?>
</body>

</html>
