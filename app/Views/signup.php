<!doctype html>
<html>

<head>
	<meta charset="UTF-8" />
	<title>signup</title>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.css" rel="stylesheet"/>
	<script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.js"></script>
</head>

<body>
	<?= session()->getFlashdata("error") ?>
	<?= service("validation")->listErrors() ?>
    <?= $this->include('header') ?>
	<h2>sign up</h2>
	<?= form_open("/signup", ["class" => "ui form"]) ?>
	<? if (isset($invitation_id)) :?>
		<?= form_hidden("invitation_id", $invitation_id) ?>
	<? endif; ?>
	<div class="field">
		<?= form_label("user_id") ?>
		<?= form_input("user_id", "", ["autocomplete" => "username"]) ?>
	</div>
	<div class="field">
		<?= form_label("password") ?>
		<?= form_password("password", "", ["autocomplete" => "new-password"]) ?>
	</div>
	<div class="field">
		<?= form_label("name") ?>
		<?= form_input("name", "") ?>
	</div>
	<?= form_submit("submit", "登録", ["class" => "ui primary button"]) ?>
	<?= form_close() ?>
</body>

</html>
