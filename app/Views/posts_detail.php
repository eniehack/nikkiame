<?php use App\ThirdParty\PostScope; ?>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title><?= $post["title"] ?></title>
	    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
	    <link href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.css" rel="stylesheet"/>
	    <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.js"></script>
	    <script src="https://unpkg.com/alpinejs@3.9.1/dist/cdn.min.js" defer></script>
    </head>
    <body>
        <?= $this->include('header') ?>
        <main class="ui container text">
            <article>
                <h1><?= $post["title"] ?></h1>
                <div>
                    <?php if ( $post["scope"] === PostScope::Passworded()->getValue() && $session_user === $post["author"] ) : ?>
                        <div class="ui warning icon message">
                            <i class="exclamation triangle icon"></i>
                            <p>
                                この投稿は<?= anchor(current_url(), "リンク") ?>を知っている人のみが閲覧できます。
                            </p>
                        </div>
                    <?php endif; ?>
                    <?= anchor("/@{$user['user_id']}", $user["name"]) ?>
                    <time><?= $post["created_at"] ?></time>
                    <?php if ($session_user === $user['ulid']): ?>
                        <a class="small ui basic button" href="<?= site_url("/@{$user['user_id']}/p/{$post['id']}/edit") ?>">
                            <i class="pencil icon"></i>
                            edit
                        </a>
                        <button id="delete-this-post" class="small negative ui button">
                            <i class="trash alternate icon"></i>
                            delete
                        </button>
                    <?php endif; ?>
                </div>
                <div>
                <?= $converter->convertToHtml($post["content"]) ?>
                </div>
            </article>
        </main>
        <script>
         $("#delete-this-post").on("click", function (event) {
             if (window.confirm("proceed to delete this posts?")) {
                 fetch("<?= site_url("/api/v0/posts/{$post['id']}") ?>", {
                     method: "GET",
                 }).then(resp => {
                     if (resp.status === 200) {
                         window.location.href = "<?= site_url("/@{$user['user_id']}") ?>"
                     } else {
                         $('body').toast({
                             class: 'warning',
                             showIcon: "exclamation triangle",
                             message: 'deletion failed: ' + resp.status,
                         });
                     }
                 })
             }
         });
        </script>
    </body>
</html>
