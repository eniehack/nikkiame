<?php

namespace App\Models;

use CodeIgniter\Model;
use App\ThirdParty\PostScope;
use Ulid\Ulid;

class Posts extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'posts';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = false;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ["title", "content", "is_draft"];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [
        "title" => "max_length[20]",
    ];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = ["generateId", "hashPassword"];
    protected $afterInsert    = [];
    protected $beforeUpdate   = ["hashPassword"];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    protected function generateId(array $data) {
        if ( isset($data["data"]["id"]) ) {
            return $data;
        }
        $data["data"]["id"] = (string) new Ulid();
        return $data;
    }

    protected function hashPassword(array $data) {
        if ( !isset($data["data"]["password"]) ) {
            return $data;
        }
        $data["data"]["password"] = password_hash($data["data"]["password"], PASSWORD_ARGON2ID);
        return $data;
    }
}
