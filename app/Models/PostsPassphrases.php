<?php

namespace App\Models;

use CodeIgniter\Model;

class PostsPassphrases extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'posts_passphrases';
    protected $primaryKey       = 'post_id';
    protected $useAutoIncrement = false;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $updatedField  = 'updated_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = ["insertUpdatedTime", "hashPassword"];
    protected $afterInsert    = [];
    protected $beforeUpdate   = ["insertUpdatedTime", "hashPassword"];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    protected function insertUpdatedTime(array $data) {
        if ( isset($data["data"]["updated_at"]) ) {
            return $data;
        }

        $data["data"]["updated_at"] = (new \DateTime())->format(\DateTime::ATOM);

        return $data;
    }

    protected function hashPassword(array $data) {
        if ( ! isset($data["data"]["passphrase"]) ) {
            return $data;
        }

        $data["data"]["passphrase"] = password_hash($data["data"]["passphrase"], PASSWORD_ARGON2ID);

        return $data;
    }
}
