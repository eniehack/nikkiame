<?php

namespace App\Models;

use CodeIgniter\Model;
use Ulid\Ulid;
use Faker\Generator;
use DateTime;

class Users extends Model
{
    protected $table            = 'users';
    protected $primaryKey       = 'ulid';
    protected $useAutoIncrement = false;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = true;
    protected $protectFields    = true;
    protected $allowedFields    = ["user_id", "name", "password"];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [
        "user_id" => [
            "rules" => "user_id|is_unique[users.user_id]|min_length[1]|max_length[15]",
            "errors" => [
                "min_length[1]" => "user_idは1~15文字の範囲に収まる必要があります",
                "max_length[15]" => "user_idは1~15文字の範囲に収まる必要があります",
                "user_id" => "user_idは数字(0~9)、アルファベット(a~z)、ドット(.)、アンダーバー(_)の中から構成される必要があります",

            ]
        ],
        "name" => "max_length[20]",
    ];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = ["generateId", "hashPassword"];
    protected $afterInsert    = [];
    protected $beforeUpdate   = ["hashPassword"];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    protected function generateId(array $data) {
        $data["data"]["ulid"] = (string) new Ulid();
        return $data;
    }

    protected function hashPassword(array $data) {
        if ( !isset($data["data"]["password"]) ) {
            return $data;
        }
        $data["data"]["password"] = password_hash($data["data"]["password"], PASSWORD_ARGON2ID);
        return $data;
    }

    public function fake(Generator &$faker) {
        $now = new DateTime();
        $user = [
            "ulid" => $faker->regexify("[^0123456789ABCDEFGHJKMNPQRSTVWXYZ]{26}"),
            "user_id" => $faker->regexify("[^0-9a-z\._]{1,15}"),
            "name" => $faker->name,
            "password" => password_hash($faker->password, PASSWORD_ARGON2ID),
            "created_at" => $now->format(DateTime::ATOM),
            "updated_at" => $now->format(DateTime::ATOM),
        ];
        return $user;
    }
}
