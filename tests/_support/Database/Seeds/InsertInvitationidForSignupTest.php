<?php

namespace Tests\Support\Database\Seeds;

use CodeIgniter\Database\Seeder;
use DateTime;

class InsertInvitationidForSignupTest extends Seeder
{
    public function run()
    {
        $factories = [
            [
                'id'    => 'aaaaaaaaaaaaaaaaaaaaaaaaaa',
                'expired_at'   => (new DateTime())->modify("+1 day")->format(DateTime::ATOM),
                'is_expired'    => false,
                "created_at" => (new DateTime())->format(DateTime::ATOM),
                "updated_at" => (new DateTime())->format(DateTime::ATOM),
            ],
            [
                'id'    => 'bbbbbbbbbbbbbbbbbbbbbbbbbb',
                'expired_at'   => (new DateTime())->modify("-1 day")->format(DateTime::ATOM),
                'is_expired'    => false,
                "created_at" => (new DateTime())->format(DateTime::ATOM),
                "updated_at" => (new DateTime())->format(DateTime::ATOM),
            ],
            [
                'id'    => 'cccccccccccccccccccccccccc',
                'expired_at'   => (new DateTime())->modify("+1 day")->format(DateTime::ATOM),
                'is_expired'    => true,
                "created_at" => (new DateTime())->format(DateTime::ATOM),
                "updated_at" => (new DateTime())->format(DateTime::ATOM),
            ],
        ];

        $builder = $this->db->table('invitations');

        foreach ($factories as $factory) {
            $builder->insert($factory);
        }
    }
}
