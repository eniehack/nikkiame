<?php

namespace Tests\App\Controllers;

use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\URI;
use CodeIgniter\Test\ControllerTestTrait;
use CodeIgniter\Test\DatabaseTestTrait;
use CodeIgniter\Test\CIUnitTestCase;
use CodeIgniter\Test\Fabricator;
use Config\App;
use App\Models\Users;

trait SigninTrait {
    protected function setupSigninTrait(): array {
        $fabricator = new Fabricator(Users::class);
        $user = $fabricator->make();
        return $user;
    }
}

class TestSigninController extends CIUnitTestCase {
    use ControllerTestTrait, DatabaseTestTrait, SigninTrait;

    protected $namespace = "App";
    protected $migrate = true;

    public function testSigninIndex() {
        $result = $this->withURI("http://example.com/signin")
            ->controller(\App\Controllers\Signin::class)
            ->execute("index");

        $result->assertOK();
    }

    public function testSignin() {
        $user = $this->setupSigninTrait();

        $body = http_build_query([
            "user_id" => $user["user_id"],
            "password" => $user["password"]
        ]);
        $result = $this->withBody($body)
                       ->withURI("http://example.com/signin")
                       ->controller(\App\Controllers\Signin::class)
                       ->execute("check");

        $result->assertOK();
    }

    public function testNoExistUserSignin() {
        $user = $this->setupSigninTrait();

        $body = http_build_query([
            "user_id" => str_shuffle($user["user_id"]),
            "password" => $user["password"],
        ]);
        $result = $this->withBody($body)
                       ->withURI("http://example.com/signin")
                       ->controller(\App\Controllers\Signin::class)
                       ->execute("check");
        $result->assertStatus(400);
    }

    public function testNoUseridSignin() {
        $user = $this->setupSigninTrait();

        $body = http_build_query([
            "user_id" => "",
            "password" => $user["password"]
        ]);
        $result = $this->withBody($body)
                       ->withURI("http://example.com/signin")
                       ->controller(\App\Controllers\Signin::class)
                       ->execute("check");
        $result->assertStatus(400);
    }

    public function testNoPasswordSignin() {
        $user = $this->setupSigninTrait();

        $body = http_build_query([
            "user_id" => $user["user_id"],
            "password" => "",
        ]);
        $result = $this->withBody($body)
                       ->withURI("http://example.com/signin")
                       ->controller(\App\Controllers\Signin::class)
                       ->execute("check");
        $result->assertStatus(400);
    }
}
