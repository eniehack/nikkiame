<?php

namespace Tests\App\Controllers;

use CodeIgniter\Test\ControllerTestTrait;
use CodeIgniter\Test\DatabaseTestTrait;
use CodeIgniter\Test\CIUnitTestCase;
use Config\App;
use Tests\Support\Database\Seeds\InsertInvitationidForSignupTest;

class TestSignupController extends CIUnitTestCase {
    use ControllerTestTrait, DatabaseTestTrait;

    protected $namespace = "App";
    protected $migrate = true;

    protected $basePath = TESTPATH . "_support/Database";
    protected $seed = InsertInvitationidForSignupTest::class;

    public function testOpenRegisterSignup() {
        $config = new App();
        $config->openRegister = true;
        $result = $this->withConfig($config)
                       ->withURI("http://example.com/signup")
                       ->controller(\App\Controllers\Signup::class)
                       ->execute("index");
        $result->assertOK();
    }

    public function testNoNewRegisterSignup() {
        $config = new App();
        $config->openRegister = false;
        $config->openInvitationRegister = false;
        echo var_dump($config);
        $result = $this->withConfig($config)
                       ->withURI("http://example.com/signup")
                       ->controller(\App\Controllers\Signup::class)
                       ->execute("index");
        $result->assertStatus(404);
    }

    public function testNoInvitationidSignup() {
        $config = new App();
        $config->openRegister = false;
        $config->openInvitationRegister = true;
        $result = $this->withConfig($config)
                       ->withURI("http://example.com/signup")
                       ->controller(\App\Controllers\Signup::class)
                       ->execute("index");
        $result->assertStatus(403);
    }

    public function testExpiredInvitationidSignup1() {
        $config = new App();
        $config->openRegister = false;
        $config->openInvitationRegister = true;
        $result = $this->withConfig($config)
                       ->withURI("http://example.com/signup?invitation_id=bbbbbbbbbbbbbbbbbbbbbbbbbb")
                       ->controller(\App\Controllers\Signup::class)
                       ->execute("index");
        $result->assertStatus(403);
    }

    public function testExpiredInvitationidSignup2() {
        $config = new App();
        $config->openRegister = false;
        $config->openInvitationRegister = true;
        $result = $this->withConfig($config)
                       ->withURI("http://example.com/signup?invitation_id=cccccccccccccccccccccccccc")
                       ->controller(\App\Controllers\Signup::class)
                       ->execute("index");
        $result->assertStatus(403);
    }

    public function testInvalidInvitationidSignup() {
        $config = new App();
        $config->openRegister = false;
        $config->openInvitationRegister = true;
        $result = $this->withConfig($config)
                       ->withURI("http://example.com/signup?invitation_id=dddddddddddddddddddddddddd")
                       ->controller(\App\Controllers\Signup::class)
                       ->execute("index");
        $result->assertStatus(403);
    }

    public function testSignupWithInvitationid() {
        $config = new App();
        $config->openRegister = false;
        $config->openInvitationRegister = true;
        $result = $this->withConfig($config)
                       ->withURI("http://example.com/signup?invitation_id=aaaaaaaaaaaaaaaaaaaaaaaaaa")
                       ->controller(\App\Controllers\Signup::class)
                       ->execute("index");
        $result->assertOK();
    }
}
